<?php

namespace Entidades\Loja\Produto;

use Entidades\Repositorio\Repositorio;

/**
 * Description of Produto
 * @Entity
 * @Table(name="produto")
 * @author Varejão
 */
class Produto {

    /**
     * @var int
     * @Id @Column(type="integer",name="id", nullable=false) 
     * @GeneratedValue(strategy="AUTO") 
     */
    private $id;

    /** @Column(type="string", length=255) */
    private $nome;

    /** @Column(type="decimal", precision=10, scale=2) */
    private $valorUnitario;

    /** @Column(type="integer") */
    private $quantidade;

    /**
     * @Column(type="datetime")
     * @Version
     */
    private $version;

    function __construct($nome, $valorUnitario, $quantidade = 1) {
        $this->nome = $nome;
        $this->valorUnitario = $valorUnitario;
        $this->quantidade = $quantidade;
    }

    function getNome() {
        return $this->nome;
    }

    function getValorUnitario() {
        return $this->valorUnitario;
    }

    function getQuantidade() {
        return $this->quantidade;
    }

    function getValorTotal() {
        return $this->valorUnitario * $this->quantidade;
    }

    /**
     * Persistindo os dados na camada repositório
     */
    public function salvar() {
        return Repositorio::salvar($this);
    }

}
