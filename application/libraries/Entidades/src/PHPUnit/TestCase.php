<?php

namespace CI\PHPUnit\Test;

use PHPUnit_Framework_TestCase as PHPUnit;

/**
 * Description of TestCase
 *
 * @author Varejão
 */
class TestCase extends PHPUnit {

    protected function setUp() {
        parent::setUp();
    }

    protected function tearDown() {
        parent::tearDown();
    }

}
