<?php

namespace Entidades\Loja\Carrinho;

use Entidades\Loja\Produto\Produto,
    Entidades\Repositorio\Repositorio,
    Doctrine\Common\Collections\ArrayCollection;

/**
 * Description of CarrinhoDeCompras
 * @Entity
 * @Table(name="carrinho")
 * @NamedNativeQueries({
 *      @NamedNativeQuery(
 *          name            = "count",
 *          resultSetMapping= "mappingCount",
 *          query           = "SELECT COUNT(*) AS count FROM carrinho"
 *      )
 * })
 * @SqlResultSetMappings({
 *      @SqlResultSetMapping(
 *          name    = "mappingCount",
 *          columns = {
 *              @ColumnResult(
 *                  name = "count"
 *              )
 *          }
 *      )
 * })
 * @author Varejão
 */
class CarrinhoDeCompras {

    /**
     * @Id @Column(type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    private $id = 0;

    /**
     * @Column(type="datetime")
     * @Version
     */
    private $version;

    /**
     * @ManyToMany(targetEntity="Entidades\Loja\Produto\Produto",cascade={"persist"})
     * @JoinTable(name="carrinho_produtos",
     *      joinColumns={@JoinColumn(name="carrinho_id", referencedColumnName="id")}
     *      )
     * */
    private $produtos;

    function __construct() {
        $this->produtos = new ArrayCollection();
    }

    public function adiciona(Produto $produto) {
        $this->produtos->add($produto);
        return $this;
    }

    public function getProdutos() {
        return $this->produtos;
    }

    public function maiorValor() {
        if (count($this->getProdutos()) === 0) {
            return 0;
        }

        $maiorValor = $this->getProdutos()[0]->getValorTotal();
        foreach ($this->getProdutos() as $produto) {
            if ($maiorValor < $produto->getValorTotal()) {
                $maiorValor = $produto->getValorTotal();
            }
        }
        return $maiorValor;
    }

    /**
     * Persistindo os dados na camada repositório
     */
    public function salvar() {
        return Repositorio::salvar($this);
    }

}
