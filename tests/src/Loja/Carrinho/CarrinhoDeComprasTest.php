<?php

namespace Entidades\Loja\Carrinho;

use Entidades\Loja\Test\TestCase,
    Entidades\Loja\Produto\Produto,
    Entidades\Repositorio\Repositorio;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CarrinhoDeComprasTest
 *
 * @author Varejão
 */
class CarrinhoDeComprasTest extends TestCase {

    public function testDeveRetornarZeroSeCarrinhoVazio() {
        $carrinho = new CarrinhoDeCompras();

        $valor = $carrinho->maiorValor();

        $this->assertEquals(0, $valor, null, 0.0001);
    }

    public function testDeveRetornarTrueSeSalvar() {
        $carrinho = new CarrinhoDeCompras();

        //Geladeira - 450.00 - 1
        $carrinho->adiciona(Repositorio::get_instance()->find("Entidades\Loja\Produto\Produto", 1));

        $valor = $carrinho->salvar();

        $this->assertEquals(TRUE, $valor);
    }

    public function testDeveRetornarMaiorValorDoCarrinhoEspecificoSalvoEmBanco() {
        $carrinho = Repositorio::get_instance()->find('Entidades\Loja\Carrinho\CarrinhoDeCompras', 1);

        $valor = $carrinho->maiorValor();

        $this->assertEquals(450.00, $valor, null, 0.0001);
    }

    public function testCountCarrinhosSalvosEmBanco() {

        $repositorio = Repositorio::get_instance()->getRepository('Entidades\Loja\Carrinho\CarrinhoDeCompras');
        $query = $repositorio->createNativeNamedQuery('count');
        $valor = end($query->getResult());

//        $this->assertEquals(25, $valor['count'], null, 0.0001);
    }

}
