<?php

namespace Entidades\Repositorio;

// o Doctrine utiliza namespaces em sua estrutura, por isto estes uses
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

define('ENTIDADES', str_replace('Repositorio', '', dirname(__FILE__)));

/**
 * Description of Doctrine
 *
 * @author Varejão
 */
class Repositorio {

    /**
     * onde irão ficar as entidades do projeto? Defina o caminho aqui
     */
    private static $entidades = [
        ENTIDADES
    ];

    /**
     *
     * @var Boolean 
     */
    const IS_DEV_MOD = \TRUE;

    /**
     * configurações de conexão. Coloque aqui os seus dados
     */
//    static $dbParams = ['driver' => 'pdo_mysql',
//        'user' => 'alcid023_user',
//        'password' => 'Alan*260189',
//        'dbname' => 'alcid023_ci',
////        'host'=> 'www.alcides-software.com'
//    ];
    private static $dbParams = ['driver' => 'pdo_mysql',
        'user' => 'root',
        'password' => '',
        'dbname' => 'alcid023_ci',
    ];

    /**
     * Entity Manager
     * @var EntityManager 
     */
    private static $_entityManager = null;

    public function __construct() {
        
    }

    public static function get_instance() {

        if (Repositorio::$_entityManager === null) {
            //setando as configurações definidas anteriormente
            $config = Setup::createAnnotationMetadataConfiguration(Repositorio::$entidades, Repositorio::IS_DEV_MOD);
            //criando o Entity Manager com base nas configurações de dev e banco de dados
            Repositorio::$_entityManager = EntityManager::create(Repositorio::$dbParams, $config);
        }

        return Repositorio::$_entityManager;
    }

    public static function salvar($entidade) {
        $em = Repositorio::get_instance();
        $em->persist($entidade);
        $em->flush();
        return true;
    }

    public static function carregar() {
        throw \IntlException;
    }

    public static function excluir($entidade) {
        throw \IntlException;
    }

}
