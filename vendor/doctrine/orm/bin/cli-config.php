<?php

use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Entidades\Repositorio\Repositorio;

// replace with file to your own project bootstrap
//require_once 'bootstrap.php';
// replace with mechanism to retrieve EntityManager in your app

//$entityManager = GetEntityManager();
$entityManager = Repositorio::get_instance();

return ConsoleRunner::createHelperSet($entityManager);
